function fish_prompt --description 'Write out the prompt'
	#Save the return status of the previous command
    set stat $status

    if not set -q __fish_prompt_normal
        set -g __fish_prompt_normal (set_color normal)
    end

    if not set -q __fish_color_blue
        set -g __fish_color_blue (set_color -o blue)
    end

    #Set the color for the status depending on the value
    set __fish_color_status (set_color -o green)
    if test $stat -gt 0
        set __fish_color_status (set_color -o red)
    end

    switch "$USER"

        case root toor

            if not set -q __fish_prompt_cwd
                if set -q fish_color_cwd_root
                    set -g __fish_prompt_cwd (set_color $fish_color_cwd_root)
                else
                    set -g __fish_prompt_cwd (set_color $fish_color_cwd)
                end
            end

            printf '%s[%s] %s%s: %s %s%s \f\r# ' (set_color F73E2E) (date "+%H:%M:%S") (set_color -o BB011D) (prompt_hostname) $USER (set_color FC041D) "$PWD"

        case '*'

            if not set -q __fish_prompt_cwd
                set -g __fish_prompt_cwd (set_color $fish_color_cwd)
            end

            printf '%s[%s] %s%s: %s %s%s \f\r# ' (set_color 55B0BA) (date "+%H:%M:%S") (set_color -o  008C9E) (prompt_hostname) $USER (set_color 00B4CC) "$PWD"

    end
end
